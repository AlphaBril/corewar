# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    install.sh                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/07/22 18:33:58 by edjubert          #+#    #+#              #
#    Updated: 2019/08/11 15:25:31 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SCRIPTS_PATH="$HOME/Documents/rendu/algo/corewar/corewar.io/install_scripts"

source "$SCRIPTS_PATH/src/error.sh"
source "$SCRIPTS_PATH/src/colors.sh"
source "$SCRIPTS_PATH/src/variables.sh"

source "$SCRIPTS_PATH/src/copy.sh"
source "$SCRIPTS_PATH/src/compile.sh"
source "$SCRIPTS_PATH/src/buildProject.sh"
source "$SCRIPTS_PATH/src/createCorFiles.sh"
source "$SCRIPTS_PATH/src/printUsage.sh"
source "$SCRIPTS_PATH/src/start.sh"

while getopts "hbcr" options; do
	case "${options}" in
		h)
			usage
			exit
			;;
		c)
			c=1
			;;
		r)
			r=1
			;;
		b)
			b=1
			;;
		*)
			error
			usage
			exit
			;;
	esac
done

shift $((OPTIND-1))

if (( c == 1 )); then
	copy
	compile
	createCorFiles
fi

if (( b == 1 )); then
	buildProject
fi

if (( r == 1 )); then
	killall node
	rm "$REACT_PATH/nohup.out"
	rm "$NODE_PATH/nohup.out"
fi

start
