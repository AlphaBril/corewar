# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    printUsage.sh                                      :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/06 13:02:57 by edjubert          #+#    #+#              #
#    Updated: 2019/08/10 16:24:36 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

usage() {
	printf "Usage: sh start.sh [-hbcr]\n"
	printf "    -h: Print usage and exit\n"
	printf "    -c: Copy Corewar resources\n"
	printf "    -r: Restart servers	\n"
	printf "    -b: Install dependencies\n"
}
