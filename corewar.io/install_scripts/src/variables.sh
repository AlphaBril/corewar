# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    variables.sh                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/06 12:53:42 by edjubert          #+#    #+#              #
#    Updated: 2019/08/06 13:12:14 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NODE_ENV="development"

PROJECT_PATH="$HOME/Documents/rendu/algo/corewar"
COREWARIO="$PROJECT_PATH/corewar.io"
REACT_PATH="$COREWARIO/react"
NODE_PATH="$COREWARIO/node"

ENV_FILE=".env"
REACT_ENV_FILE="$REACT_PATH/.env"
DOCKER_NODE_PORT="4000"

RESOURCES_PATH="$COREWARIO/node/resources"
COREWAR_CHAMPS1="$RESOURCES_PATH/vm_champs/champs/championships/**/**/*.[s]"
COREWAR_CHAMPS2="$RESOURCES_PATH/vm_champs/champs/championships/**/*[s]"
COREWAR_CHAMPS3="$RESOURCES_PATH/vm_champs/champs/examples/*[s]"


DEST_PATH="$NODE_PATH/resources"
CHAMPS_PATH="$DEST_PATH/champs"
COREWAR_PATH="$DEST_PATH/corewar"
