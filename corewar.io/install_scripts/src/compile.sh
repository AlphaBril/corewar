# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    compile.sh                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/06 13:06:36 by edjubert          #+#    #+#              #
#    Updated: 2019/08/06 13:06:50 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

compile() {
	printf $YELLOW
	printf "\n\n"
	printf "Compiling sources in$RED 3\r$YELLOW"
	sleep 1
	printf "Compiling sources in$RED 2\r$YELLOW"
	sleep 1
	printf "Compiling sources in$RED 1\r$YELLOW"
	sleep 1
	printf "Compiling sources in now:\n"
	sleep 1
	make re -C $COREWAR_PATH
	printf "$GREEN DONE\n\n$EOC"
}
