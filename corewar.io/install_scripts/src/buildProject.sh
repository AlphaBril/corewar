# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    buildProject.sh                                    :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/06 13:07:57 by edjubert          #+#    #+#              #
#    Updated: 2019/08/06 13:09:11 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

buildProject() {
	printf $YELLOW
	printf "\n%-91s" "Removing node's node_modules folder"
	rm -rf "$COREWARIO/node/node_modules"
	printf "$GREEN DONE\n$EOC"

	printf $YELLOW
	printf "%-91s\n" "Installing Node modules"
	cd "$COREWARIO/node"; yarn;
	printf "$GREEN%-92s%s$EOC\n" "Installing Node project" "DONE"

	printf $YELLOW
	printf "\n%-91s" "Removing react's node_modules folder"
	rm -rf "$COREWARIO/react/node_modules"
	printf "$GREEN DONE\n$EOC"

	printf $YELLOW
	printf "%-91s\n" "Installing React modules"
	cd "$COREWARIO/react"; yarn;
	printf "$GREEN%-92s%s$EOC\n\n" "Installing React project" "DONE"
}
