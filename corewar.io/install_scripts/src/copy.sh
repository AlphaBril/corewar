# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    copy.sh                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/06 12:55:00 by edjubert          #+#    #+#              #
#    Updated: 2019/08/11 15:33:58 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

copy() {
	printf "$YELLOW%s $GREEN%-82s" "Deleting" $DEST_PATH
	rm -rf $DEST_PATH
	printf "$GREEN DONE\n$EOC"

	printf $YELLOW
	printf "%s $GREEN%s$YELLOW %s $GREEN%s $YELLOW %-32s" "Make" $CHAMPS_PATH "and" $COREWAR_PATH "folders"
	mkdir -p $CHAMPS_PATH $COREWAR_PATH
	printf "$GREEN DONE\n$EOC"

	printf $YELLOW
	printf "%s $GREEN%s$YELLOW %s $GREEN%s $YELLOW %-10s" "Copy" "$PROJECT_PATH/libft" "into" $COREWAR_PATH
	cp -rf "$PROJECT_PATH/libft" $COREWAR_PATH
	printf "$GREEN DONE\n$EOC"

	printf $YELLOW
	printf "%s $GREEN%s$YELLOW %s $GREEN%s $YELLOW %-7s" "Copy" "$PROJECT_PATH/includes" "into" $COREWAR_PATH
	cp -rf "$PROJECT_PATH/includes" $COREWAR_PATH
	printf "$GREEN DONE\n$EOC"

	printf $YELLOW
	printf "%s $GREEN%s$YELLOW %s $GREEN%s $YELLOW %-11s" "Copy" "$PROJECT_PATH/srcs" "into" $COREWAR_PATH
	cp -rf "$PROJECT_PATH/srcs" $COREWAR_PATH
	printf "$GREEN DONE\n$EOC"

	printf $YELLOW
	printf "%s $GREEN%s$YELLOW %s $GREEN%s $YELLOW %-7s" "Copy" "$PROJECT_PATH/Makefile" "into" $COREWAR_PATH
	cp -rf "$PROJECT_PATH/Makefile" $COREWAR_PATH
	printf "$GREEN DONE\n$EOC"

	printf $YELLOW
	printf "%s $GREEN%s$YELLOW %s $GREEN%s $YELLOW %s" "Copy" "../resources/vm_champs/champs/championships/**/**/*.[s]" "into" $CHAMPS_PATH
	cp -rf ../resources/vm_champs/champs/championships/**/**/*.[s] $CHAMPS_PATH
	printf "$GREEN DONE\n$EOC"

	printf $YELLOW
	printf "%s $GREEN%s$YELLOW %s $GREEN%s $YELLOW %s" "Copy" "../jeanmichaelvincent.s" "into" $CHAMPS_PATH
	cp -rf ../jeanmichaelvincent.s $CHAMPS_PATH
	printf "$GREEN DOWN\n$EOC"

	printf $YELLOW
	printf "%s $GREEN%s$YELLOW %s $GREEN%s $YELLOW %-20s" "Copy" "../resources/vm_champs/champs/*.[s]" "into" $CHAMPS_PATH
	cp -rf ../resources/vm_champs/champs/*.[s] $CHAMPS_PATH
	printf "$GREEN DONE\n$EOC"

	printf $YELLOW
	printf "%s $GREEN%s$YELLOW %s $GREEN%s $YELLOW %-11s" "Copy" "../resources/vm_champs/champs/examples/*.[s]" "into" $CHAMPS_PATH
	cp -rf ../resources/vm_champs/champs/examples/*.[s] $CHAMPS_PATH
	printf "$GREEN DONE\n$EOC"

}
