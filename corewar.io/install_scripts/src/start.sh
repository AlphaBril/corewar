# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    start.sh                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/06 13:02:18 by edjubert          #+#    #+#              #
#    Updated: 2019/08/06 13:09:58 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

start() {
	printf "$YELLOW%-91s" "Running node server"
	cd "$COREWARIO/node"; nohup yarn start &
	printf "$GREEN DONE\n$EOC"

	printf "$YELLOW%-91s" "Running react server"
	cd "$COREWARIO/react"; nohup yarn start &
	printf "$GREEN DONE\n$EOC"
}
