# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    createCorFiles.sh                                  :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/06 13:07:21 by edjubert          #+#    #+#              #
#    Updated: 2019/08/06 13:07:27 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

createCorFiles() {
	for file in $CHAMPS_PATH/*.s
	do
		printf "$YELLOW%-92s%s\r$EOC" $file "Compiling..."
		output=$($COREWAR_PATH/asm $file)
		if [[ $output == *"Writing output"* ]]; then
			printf "%-120s\r" ""
			printf "$GREEN%-92s$GREEN%s\n$EOC" $file "DONE"
		else
			printf "%-120s\r" ""
			printf "$RED%-92s$RED%s\n$EOC" $file "ERROR"
			rm -rf $file
		fi
	done
}
